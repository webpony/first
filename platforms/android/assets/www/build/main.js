webpackJsonp([13],{

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpPage');
    };
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/help/help.html"*/'<!--\n  Generated template for the HelpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>help</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/help/help.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__suitable_suitable__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.pushPage = __WEBPACK_IMPORTED_MODULE_5__suitable_suitable__["a" /* SuitablePage */];
        this.SwipedTabsIndicator = null;
        this.tabs = [];
        this.data7$ = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/creditallceli/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dengi_podzalog/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgispisanie/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.loadData();
    }
    /*
      Открытие страницы оформления займа ТЕСТ
       */
    HomePage.prototype.openPodhzayvkaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]);
    };
    HomePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json');
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    //Скрол начало
    HomePage.prototype.doInfinite = function () {
        var _this = this;
        console.log('Begin async operation');
        return new Promise(function (resolve) {
            setTimeout(function () {
                for (var i = 0; i < 30; i++) {
                    _this.items.push(_this.items.length);
                }
                console.log('Async operation has ended');
                resolve();
            }, 500);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>Home</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  \n  <ion-content padding >\n     \n    \n    <br>\n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Подходящее для вас</h4>\n  \n  \n  \n    <ion-scroll scrollX="true" style="white-space:nowrap;height: 150px;background-color: #ffffff;">\n     \n   \n    \n      <ion-card  style="background-color:#ffffff">\n        <img  src="./assets/slide/all/zaem.png"  tapped [navPush]="pushPage" [navParams]="{initialSlide: 0}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/credit-cart.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 1}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/credit-all.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 2}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff"> \n        <img src="./assets/slide/all/credit-zalog.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 3}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n    \n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/help-credit.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 4}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/dolg.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 5}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n    </ion-scroll>\n  \n  \n  \n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Интересное</h4>\n  \n    <ion-scroll scrollX="true" style="white-space:nowrap;width:100%;height: 185px;background-color: #ffffff;">\n      <ion-card class="card_int" nowrap *ngFor="let item of items" style="width:100% ;padding:10px;height:100%"><h4 text-center style="font-size: 1.2em; font-weight: 600;">{{item.company_h}}</h4>\n        <ion-grid>\n          <ion-row text-center>\n            <ion-col col-md-3>\n           18+\n            </ion-col>\n            <ion-col >\n                <ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon>\n                <ion-icon name="star-outline"></ion-icon>\n            </ion-col>\n            <ion-col col-md-3 offset-md-3>\n             <ion-icon name="heart"></ion-icon>\n            </ion-col>\n          </ion-row>\n          <button  style ="text-align: center;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button color="secondary">Оформить</button>\n          <h3 text-center>{{item.link_company_h}}</h3>\n          <h6 text-center>{{item.int_info_h}}</h6>\n    \n    \n        </ion-grid>\n      </ion-card>\n        </ion-scroll>\n  \n  \n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Все категории</h4>\n  \n    <ion-grid>\n      <ion-row justify-content-start>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 0}">\n          <img src="./assets/slide/all/card.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 1}">\n          <img src="./assets/slide/all/card2.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 2}">\n          <img src="./assets/slide/all/card3.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 3}">\n          <img src="./assets/slide/all/card4.png">\n        </ion-col>\n        <ion-col col-6 col-sm  tapped [navPush]="pushPage" [navParams]="{initialSlide: 4}">\n          <img src="./assets/slide/all/card5.png">\n        </ion-col>\n        <ion-col col-6 col-sm  tapped [navPush]="pushPage" [navParams]="{initialSlide: 5}">\n          <img src="./assets/slide/all/card6.png">\n        </ion-col>\n  \n  \n  \n      </ion-row>\n    </ion-grid>\n    \n  <!--Скрол начало-->\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n        <ion-infinite-scroll-content\n          loadingSpinner="bubbles"\n          loadingText="Хватит тянуть меня! Растянешь! :))...">\n        </ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n  \n  <!--Скрол конец-->\n  \n  \n  </ion-content>'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var RegisterPage = /** @class */ (function () {
    function RegisterPage(ofAuth, navCtrl, navParams) {
        this.ofAuth = ofAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
    }
    RegisterPage.prototype.register = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.ofAuth.auth.createUserWithEmailAndPassword(user.email, user.password)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/register/register.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Register</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating>Email</ion-label>\n    <ion-input type="text" [(ngModel)]="user.email"></ion-input>\n    </ion-item>\n    \n    <ion-item>\n        <ion-label floating>Пароль</ion-label>\n        <ion-input type="password" [(ngModel)]="user.password"></ion-input>\n        </ion-item>\n        <button ion-button (click)="register(user)">Регистрация</button>\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessagesPage = /** @class */ (function () {
    function MessagesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-messages',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/messages/messages.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Выбрать\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n \n \n      <ion-select  class="region" placeholder="Выберите регион" (ionChange)="setDistrictValues(sState)" [(ngModel)]="sState">\n          <ion-option [value]="sState" *ngFor = "let sState of states">{{sState.name}}  </ion-option>\n     </ion-select>\n           \n  <br>\n <ion-list *ngIf="selectedDistricts">\n       \n    <ion-select  class="region" placeholder="Выберите регион" (ionChange)="setCityValues(sDistrict)" [(ngModel)]="sDistrict">\n        <ion-option [value]="sDistrict" *ngFor = "let sDistrict of selectedDistricts">{{sDistrict.name}}</ion-option>\n       </ion-select>\n  </ion-list>\n <ion-list *ngIf="selectedCities">\n       <ion-item *ngFor="let city of selectedCities">\n             <p>{{city.name}}</p>\n       </ion-item>\n </ion-list>\n\n  </ion-list>\n  </ion-content>\n\n\n  \n    '/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/messages/messages.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], MessagesPage);
    return MessagesPage;
}());

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>search</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlidePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SlidePage = /** @class */ (function () {
    function SlidePage(navCtrl, navParams, alerCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alerCtrl = alerCtrl;
        this.appName = 'Ionic App';
        this.langForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            "langs": new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]({ value: 'rust', disabled: false })
        });
        this.initializeState();
        this.initializeDistrict();
        this.initializeCity();
    }
    SlidePage.prototype.initializeState = function () {
        this.states = [
            { id: 1, name: 'Россия' }
            /*{id: 2, name: 'Казахстан'},
            {id: 3, name: 'Украина'} */
        ];
    };
    SlidePage.prototype.initializeDistrict = function () {
        this.districts = [
            { id: 1, name: 'Все', state_id: 1, state_name: 'Все' },
            { id: 2, name: 'Москва и область', state_id: 1, state_name: 'Москва и область' },
            { id: 3, name: 'Санкт-Петербург и область', state_id: 1, state_name: 'Санкт-Петербург и область' },
            { id: 4, name: 'Алтайский край', state_id: 1, state_name: 'Алтайский край' },
            { id: 5, name: 'Адыгея', state_id: 1, state_name: 'Адыгея' },
            { id: 6, name: 'Амурская область', state_id: 1, state_name: 'Амурская область' },
            { id: 7, name: 'Архангельская область', state_id: 1, state_name: 'Архангельская область' },
            { id: 8, name: 'Астраханская область', state_id: 1, state_name: 'Астраханская область' },
            { id: 9, name: 'Башкортостан', state_id: 1, state_name: 'Башкортостан' },
            { id: 10, name: 'Белгородская область', state_id: 1, state_name: 'Белгородская область' },
            { id: 11, name: 'Брянская область', state_id: 1, state_name: 'Брянская область' },
            { id: 12, name: 'Бурятия', state_id: 1, state_name: 'Бурятия' },
            { id: 13, name: 'Владимирская область', state_id: 1, state_name: 'Владимирская область' },
            { id: 14, name: 'Волгоградская область', state_id: 1, state_name: 'Волгоградская область' },
            { id: 15, name: 'Вологодская область', state_id: 1, state_name: 'Вологодская область' },
            { id: 16, name: 'Воронежская область', state_id: 1, state_name: 'Воронежская область' },
            { id: 17, name: 'Дагестан', state_id: 1, state_name: 'Дагестан' },
            { id: 18, name: 'Еврейская область', state_id: 1, state_name: 'Еврейская область' },
            { id: 19, name: 'Забайкальский край', state_id: 1, state_name: 'Забайкальский край' },
            { id: 20, name: 'Ивановская область', state_id: 1, state_name: 'Ивановская область' },
            { id: 21, name: 'Ингушетия', state_id: 1, state_name: 'Ингушетия' },
            { id: 22, name: 'Иркутская область', state_id: 1, state_name: 'Иркутская область' },
            { id: 23, name: 'Кабардино-Балкария', state_id: 1, state_name: 'Кабардино-Балкария' },
            { id: 24, name: 'Калининградская область', state_id: 1, state_name: 'Калининградская область' },
            { id: 25, name: 'Калмыкия', state_id: 1, state_name: 'Калмыкия' },
            { id: 26, name: 'Калужская область', state_id: 1, state_name: 'Калужская область' },
            { id: 27, name: 'Камчатский край', state_id: 1, state_name: 'Камчатский край' },
            { id: 28, name: 'Карачаево-Черкессия', state_id: 1, state_name: 'Карачаево-Черкессия' },
            { id: 29, name: 'Карелия', state_id: 1, state_name: 'Карелия' },
            { id: 30, name: 'Кемеровская область', state_id: 1, state_name: 'Кемеровская область' },
            { id: 31, name: 'Кировская область', state_id: 1, state_name: 'Кировская область' },
            { id: 32, name: 'Костромская область', state_id: 1, state_name: 'Костромская область' },
            { id: 33, name: 'Краснодарский край', state_id: 1, state_name: 'Краснодарский край' },
            { id: 34, name: 'Красноярский край', state_id: 1, state_name: 'Красноярский край' },
            { id: 35, name: 'Курганская область', state_id: 1, state_name: 'Курганская область' },
            { id: 36, name: 'Курская область', state_id: 1, state_name: 'Курская область' },
            { id: 37, name: 'Липецкая область', state_id: 1, state_name: 'Липецкая область' },
            { id: 38, name: 'Магаданская область', state_id: 1, state_name: 'Магаданская область' },
            { id: 39, name: 'Марий Эл', state_id: 1, state_name: 'Марий Эл' },
            { id: 40, name: 'Питер', state_id: 1, state_name: 'Питер' },
            { id: 41, name: 'Мордовия', state_id: 1, state_name: 'Мордовия' },
            { id: 42, name: 'Мурманская область', state_id: 1, state_name: 'Мурманская область' },
            { id: 43, name: 'Ненецкий округ', state_id: 1, state_name: 'Ненецкий округ' },
            { id: 44, name: 'Нижегородская область', state_id: 1, state_name: 'Нижегородская область' },
            { id: 45, name: 'Новгородская область', state_id: 1, state_name: 'Новгородская область' },
            { id: 46, name: 'Новосибирская область', state_id: 1, state_name: 'Новосибирская область' },
            { id: 47, name: 'Омская область', state_id: 1, state_name: 'Омская область' },
            { id: 48, name: 'Оренбургская область', state_id: 1, state_name: 'Оренбургская область' },
            { id: 49, name: 'Орловская область', state_id: 1, state_name: 'Орловская область' },
            { id: 50, name: 'Пензенская область', state_id: 1, state_name: 'Пензенская область' },
            { id: 51, name: 'Пермский край', state_id: 1, state_name: 'Пермский край' },
            { id: 52, name: 'Приморский край', state_id: 1, state_name: 'Приморский край' },
            { id: 53, name: 'Псковская область', state_id: 1, state_name: 'Псковская область' },
            { id: 54, name: 'Ростовская область', state_id: 1, state_name: 'Ростовская область' },
            { id: 55, name: 'Рязанская область', state_id: 1, state_name: 'Рязанская область' },
            { id: 56, name: 'Самарская область', state_id: 1, state_name: 'Самарская область' },
            { id: 57, name: 'Саратовская область', state_id: 1, state_name: 'Саратовская область' },
            { id: 58, name: 'Сахалинская область', state_id: 1, state_name: 'Сахалинская область' },
            { id: 59, name: 'Свердловская область', state_id: 1, state_name: 'Свердловская область' },
            { id: 60, name: 'Северная Осетия', state_id: 1, state_name: 'Северная Осетия' },
            { id: 61, name: 'Северная Осетия', state_id: 1, state_name: 'Северная Осетия' },
            { id: 62, name: 'Ставропольский край', state_id: 1, state_name: 'Ставропольский край' },
            { id: 63, name: 'Тамбовская область', state_id: 1, state_name: 'Тамбовская область' },
            { id: 64, name: 'Татарстан', state_id: 1, state_name: 'Татарстан' },
            { id: 65, name: 'Тверская область', state_id: 1, state_name: 'Тверская область' },
            { id: 66, name: 'Томская область', state_id: 1, state_name: 'Томская область' },
            { id: 67, name: 'Тульская область', state_id: 1, state_name: 'Тульская область' },
            { id: 68, name: 'Тыва', state_id: 1, state_name: 'Тыва' },
            { id: 69, name: 'Тюменская область', state_id: 1, state_name: 'Тюменская область' },
            { id: 70, name: 'Удмуртия', state_id: 1, state_name: 'Удмуртия' },
            { id: 71, name: 'Ульяновская область', state_id: 1, state_name: 'Ульяновская область' },
            { id: 72, name: 'Хабаровский край', state_id: 1, state_name: 'Хабаровский край' },
            { id: 73, name: 'Хакасия', state_id: 1, state_name: 'Хакасия' },
            { id: 74, name: 'Ханты-Мансийский округ', state_id: 1, state_name: 'Ханты-Мансийский округ' },
            { id: 75, name: 'Челябинская область', state_id: 1, state_name: 'Челябинская область' },
            { id: 76, name: 'Чечня', state_id: 1, state_name: 'Чечня' },
            { id: 77, name: 'Чувашия', state_id: 1, state_name: 'Чувашия' },
            { id: 78, name: 'Чукотский округ', state_id: 1, state_name: 'Чукотский округ' },
            { id: 79, name: 'Якутия', state_id: 1, state_name: 'Якутия' },
            { id: 80, name: 'Ямало-Ненецкий округ', state_id: 1, state_name: 'Ямало-Ненецкий округ' },
            { id: 81, name: 'Ярославская область', state_id: 1, state_name: 'Ярославская область' },
        ];
    };
    SlidePage.prototype.initializeCity = function () {
        this.cities = [
            { id: 1, name: 'City of Alor Gajah 1', state_id: 1, district_id: 1 },
            { id: 2, name: 'City of Alor Gajah 2', state_id: 1, district_id: 1 },
            { id: 3, name: 'City of Jasin 1', state_id: 1, district_id: 2 },
            { id: 4, name: 'City of Muar 1', state_id: 2, district_id: 3 },
            { id: 5, name: 'City of Muar 2', state_id: 2, district_id: 3 },
            { id: 6, name: 'City of Segamat 1', state_id: 2, district_id: 4 },
            { id: 7, name: 'City of Shah Alam 1', state_id: 3, district_id: 5 },
            { id: 8, name: 'City of Klang 1', state_id: 3, district_id: 6 },
            { id: 9, name: 'City of Klang 2', state_id: 3, district_id: 6 }
        ];
    };
    SlidePage.prototype.setDistrictValues = function (sState) {
        this.selectedDistricts = this.districts.filter(function (district) { return district.state_id == sState.id; });
    };
    SlidePage.prototype.setCityValues = function (sDistrict) {
        this.selectedCities = this.cities.filter(function (city) { return city.district_id == sDistrict.id; });
    };
    SlidePage.prototype.doSubmit = function (event) {
        console.log('Submitting form', this.langForm.value);
        event.preventDefault();
    };
    SlidePage.prototype.goAnOtherPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    SlidePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-slide',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/slide/slide.html"*/'\n\n\n<ion-content  >\n<ion-slides pager>\n\n    <ion-slide style="background-color: rgb(25, 25, 25); ">\n<div class="text_slide">\n  <div class="hello"> Привет! \n  	\n  </div>\n  \n  <h5 style="color:white;">Мы поможем тебе <br>\n  легко найти деньги</h5>\n\n<br>\n<div class="logo_slide">\n  <img src="./assets/imgs/logo_slide2.png">\n</div>\n<br>\n\n<h5 style="color:white;">Для поиска денег<br>нажмите далее</h5>\n<br>\n<button ion-button color="light" >Далее</button>\n</div>\n</ion-slide>\n\n\n\n\n<ion-slide #slide2 style="background-color: rgb(25, 25, 25);">\n\n\n<br>\n  <ion-label style="color:#ffffff;  font-size: 1.2em; font-weight: 600;">Я ищу</ion-label>\n  <ion-item style="background-color: rgb(25, 25, 25);">\n  <ion-select class="region" placeholder="Выберите параметр">\n    <ion-option value="mond">Деньги без залога</ion-option>\n    <ion-option value="monp">Деньги под залог</ion-option>\n    <ion-option value="cred">Помощь в кредите</ion-option>\n    <ion-option value="nom">Избавление от кредитов</ion-option>\n    <ion-option value="allvs">Рассмотрю все варианты</ion-option>\n</ion-select>\n</ion-item>\n\n\n\n\n\n\n\n<br>\n  <ion-label style="color:#ffffff;  font-size: 1.2em; font-weight: 600;">Страна</ion-label>\n<ion-item style="background-color: rgb(25, 25, 25);">\n  <br>\n    <ion-select  class="region" placeholder="Выберите страну" (ionChange)="setDistrictValues(sState)" [(ngModel)]="sState">\n        <ion-option [value]="sState" *ngFor = "let sState of states">{{sState.name}}  </ion-option>\n   </ion-select>\n    \n</ion-item>\n<br>\n<br>\n\n<ion-label style="color:#ffffff;  font-size: 1.2em; font-weight: 600;">Регион</ion-label>\n<ion-item style="background-color: rgb(25, 25, 25);">\n  \n\n       \n    <ion-select  class="region" placeholder="Выберите регион">\n        <ion-option [value]="sDistrict" *ngFor = "let sDistrict of selectedDistricts">{{sDistrict.name}}</ion-option>\n       </ion-select>\n  \n</ion-item><br>\n<div class="hello_text">\n  <h5 style="color:white;">Выберите страну/регион <br>\n  и нажмите "Далее"</h5>\n</div>\n <br>\n <button ion-button color="light"(click)="goAnOtherPage()">ИСКАТЬ</button>\n \n\n\n\n\n\n\n\n\n</ion-slide>\n\n\n\n  \n  \n\n\n\n\n\n</ion-slides>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/slide/slide.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], SlidePage);
    return SlidePage;
}());

//# sourceMappingURL=slide.js.map

/***/ }),

/***/ 171:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 171;

/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/account/account.module": [
		701,
		12
	],
	"../pages/favorites/favorites.module": [
		702,
		11
	],
	"../pages/help/help.module": [
		703,
		10
	],
	"../pages/home/home.module": [
		704,
		9
	],
	"../pages/login/login.module": [
		705,
		8
	],
	"../pages/messages/messages.module": [
		706,
		7
	],
	"../pages/modal/modal.module": [
		707,
		1
	],
	"../pages/personal/personal.module": [
		708,
		0
	],
	"../pages/podhzayvka/podhzayvka.module": [
		709,
		6
	],
	"../pages/register/register.module": [
		710,
		5
	],
	"../pages/search/search.module": [
		711,
		4
	],
	"../pages/slide/slide.module": [
		713,
		3
	],
	"../pages/suitable/suitable.module": [
		712,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 215;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__suitable_suitable__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__help_help__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__messages_messages__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__suitable_suitable__["a" /* SuitablePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_6__messages_messages__["a" /* MessagesPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_5__help_help__["a" /* HelpPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/tabs/tabs.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Деньги в долг срочно</ion-title>\n    <ion-buttons end>  \n        <button class="contact_avatar" name="contact"(click)="openPopupPage()">\n          <ion-icon name="contact"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Домой" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Программы" tabIcon="keypad"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Поиск" tabIcon="search"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Сообщения" tabIcon="text"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="Помощь" tabIcon="medkit"></ion-tab>\n  \n</ion-tabs>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AccountPage = /** @class */ (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-account',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/account/account.html"*/'<!--\n  Generated template for the AccountPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>account</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/account/account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FavoritesPage = /** @class */ (function () {
    function FavoritesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FavoritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritesPage');
    };
    FavoritesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-favorites',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/favorites/favorites.html"*/'<!--\n  Generated template for the FavoritesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>favorites</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/favorites/favorites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FavoritesPage);
    return FavoritesPage;
}());

//# sourceMappingURL=favorites.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__register_register__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var LoginPage = /** @class */ (function () {
    function LoginPage(ofAuth, navCtrl, navParams) {
        this.ofAuth = ofAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
    }
    LoginPage.prototype.login = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    result = this.ofAuth.auth.signInWithEmailAndPassword(user.email, user.password);
                    console.log(result);
                    if (result) {
                        this.navCtrl.setRoot("PersonalPage");
                    }
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/login/login.html"*/'\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-item>\n<ion-label floating>Email</ion-label>\n<ion-input type="text" [(ngModel)]="user.email"></ion-input>\n</ion-item>\n\n<ion-item>\n    <ion-label floating>Пароль</ion-label>\n    <ion-input type="password" [(ngModel)]="user.password"></ion-input>\n    </ion-item>\n\n<button ion-button (click)="login(user)">Логин</button>\n<button ion-button  color="light" (click)="register()">Регистрация</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(365);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_firebase_config__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(696);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(700);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_help_help__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_account_account__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_register_register__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_slide_slide__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_favorites_favorites__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_search_search__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_suitable_suitable__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_messages_messages__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_podhzayvka_podhzayvka__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_common_http__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_suitable_suitable__["a" /* SuitablePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_slide_slide__["a" /* SlidePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_favorites_favorites__["a" /* FavoritesPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favorites/favorites.module#FavoritesPageModule', name: 'FavoritesPage', segment: 'favorites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help/help.module#HelpPageModule', name: 'HelpPage', segment: 'help', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/messages/messages.module#MessagesPageModule', name: 'MessagesPage', segment: 'messages', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal/modal.module#ModalPageModule', name: 'ModalPage', segment: 'modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/personal/personal.module#PersonalPageModule', name: 'PersonalPage', segment: 'personal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/podhzayvka/podhzayvka.module#PodhzayvkaPageModule', name: 'PodhzayvkaPage', segment: 'podhzayvka', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/suitable/suitable.module#SuitablePageModule', name: 'SuitablePage', segment: 'suitable', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slide/slide.module#SlidePageModule', name: 'SlidePage', segment: 'slide', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_0__app_firebase_config__["a" /* FIREBASE_CONFIG */]),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_21__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_18__pages_suitable_suitable__["a" /* SuitablePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_slide_slide__["a" /* SlidePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_favorites_favorites__["a" /* FavoritesPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyB2M6KvNtii5HuCiwVdjOjCAxadVv87S8U",
    authDomain: "fir-auth-c234c.firebaseapp.com",
    databaseURL: "https://fir-auth-c234c.firebaseio.com",
    projectId: "fir-auth-c234c",
    storageBucket: "fir-auth-c234c.appspot.com",
    messagingSenderId: "697092668767"
};
//# sourceMappingURL=app.firebase.config.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_slide_slide__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_slide_slide__["a" /* SlidePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.openPage = function (page) {
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PodhzayvkaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PodhzayvkaPage = /** @class */ (function () {
    function PodhzayvkaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PodhzayvkaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-podhzayvka',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/podhzayvka/podhzayvka.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>posdfsdfdhzayvka</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\nsadfsdfadsf\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/podhzayvka/podhzayvka.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], PodhzayvkaPage);
    return PodhzayvkaPage;
}());

//# sourceMappingURL=podhzayvka.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuitablePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SuitablePage = /** @class */ (function () {
    function SuitablePage(alertController, modalCtrl, http, navCtrl, navParams) {
        this.alertController = alertController;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SwipedTabsIndicator = null;
        this.tabs = [];
        this.initialSlide = 0;
        this.data7 = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/creditallceli/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dengi_podzalog/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgispisanie/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.tabs = ["Займы", "Карты", "Кредиты", "Деньги", "Помощь", "Долги"];
        // this.loadData (); 
        console.log(navParams);
        if (navParams.get('initialSlide')) {
            this.initialSlide = navParams.get('initialSlide');
        }
    }
    SuitablePage.prototype.openModal = function () {
        var myModalOptions = {
            enableBackdropDismiss: false
        };
        var myModalData = {
            name: 'Paul Halliday',
            occupation: 'Developer'
        };
        var myModal = this.modal.create('ModalPage', { data: myModalData }, myModalOptions);
        myModal.present();
        myModal.onDidDismiss(function (data) {
            console.log("I have dismissed.");
            console.log(data);
        });
        myModal.onWillDismiss(function (data) {
            console.log("I'm about to dismiss");
            console.log(data);
        });
    };
    SuitablePage.prototype.ionViewDidEnter = function () {
        this.SwipedTabsIndicator = document.getElementById("indicator");
    };
    SuitablePage.prototype.selectTab = function (index) {
        this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (100 * index) + '%,0,0)';
        this.SwipedTabsSlider.slideTo(index, 500);
    };
    SuitablePage.prototype.updateIndicatorPosition = function () {
        // this condition is to avoid passing to incorrect index
        if (this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
            //	this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
        }
    };
    SuitablePage.prototype.animateIndicator = function ($event) {
        if (this.SwipedTabsIndicator)
            this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress * (this.SwipedTabsSlider.length() - 1)) * 100) + '%,0,0)';
    };
    /*
      Открытие страницы оформления займа ТЕСТ
       */
    SuitablePage.prototype.openPodhzayvkaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]);
    };
    SuitablePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/credit_card/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/creditallceli/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/dengi_podzalog/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgispisanie/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json');
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim_card/.json');
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    SuitablePage.prototype.openHintModal = function (item) {
        console.log('openHintModal');
        console.log(item);
        var modalOptions = {
            cssClass: "inset-modal"
        };
        var modal = this.modalCtrl.create("ModalPage", { item: item }, modalOptions);
        modal.present();
        console.log('Cancel clicked');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('SwipedTabsSlider'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */])
    ], SuitablePage.prototype, "SwipedTabsSlider", void 0);
    SuitablePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-suitable',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/suitable/suitable.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding >\n\n\n        <ion-slides #SwipedTabsSlider  (ionSlideDrag)="animateIndicator($event)"\n                    (ionSlideWillChange)="updateIndicatorPosition()"\n                    (ionSlideDidChange)="updateIndicatorPosition()"\n                    (pan)="updateIndicatorPosition()"\n                    [initialSlide]="initialSlide"\n                    [pager]="false"\n        >\n       <!-- here is our dynamic line  "indicator"-->\n       <div id=\'indicator\'  class="SwipedTabs-indicatorSegment" [ngStyle]="{\'width.%\': (100/this.tabs.length)}"></div>\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs">\n                  <ion-segment-button  *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n      \n\n              <div class="letter_head" style="background: #006699;height: 100px;max-height: 96%;">\n                  <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                        <img src="./assets/imgs/z.png">\n                      </div></div>\n                      \n              <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Займы</h4>\n              <p text-center style="font-size: 1.0em; line-height: 1.5;">все</p>\n\n              <br> \n              <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n                <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n              </button>\n                   \n            <ion-item *ngFor="let item of (data6$ | async)">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button ion-button  (click)="openHintModal(item)" >\n                        <ion-icon name="share-alt"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="cardcom_id">{{item.id}}</div>\n                    <div class="card-zag">{{item.company_k}}</div>\n                    <div class="card-alls">{{item.int_info_k}}</div>\n                    <div class="card-all">{{item.filter_category_k}}</div>\n                \n                  <ion-buttons end>\n                  \n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n             \n             \n          <br>\n          </ion-slide>\n\n          <ion-slide >\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n    \n              <div class="letter_head" style="background: #006666;height: 100px;max-height: 96%;">\n                  <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                        <img src="./assets/imgs/kg.png">\n                      </div></div>\n                      \n              <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Карты</h4>\n              <p text-center style="font-size: 1.0em; line-height: 1.5;">все</p>\n              <br>\n              <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n                                    <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n                </button>\n                \n                <ion-item *ngFor="let item of (data1$ | async)">\n                  <ion-toolbar>\n                      <ion-buttons end>\n                          <button ion-button>\n                            <ion-icon name="information-circle"></ion-icon>\n                          </button>\n                        </ion-buttons>\n                        <div class="card-zag">{{item.company_kr}}</div>\n                        <div class="card-all">{{item.int_info_kr}}</div>\n                        <div class="card-all">{{item.filter_category_kr}}</div>\n                      <ion-buttons end>\n                        <button ion-button>\n                          <ion-icon ios="ios-call" md="md-call"></ion-icon>\n                        </button>\n                      </ion-buttons>\n                    </ion-toolbar>\n          \n           </ion-item>\n          <br>\n          </ion-slide>\n\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #006666;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/k.png">\n                  </div></div>\n                  \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Кредиты</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">кол-во предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n            <ion-item *ngFor="let item of (data1$ | async)">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button>\n                        <ion-icon name="information-circle"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="card-zag">{{item.company_kr}}</div>\n                    <div class="card-all">{{item.int_info_kr}}</div>\n                    <div class="card-all">{{item.filter_category_c}}</div>\n                  <ion-buttons end>\n                    <button ion-button>\n                      <ion-icon ios="ios-call" md="md-call"></ion-icon>\n                    </button>\n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n      <br>\n          </ion-slide>\n\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #65361b;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/d.png">\n                  </div></div>\n                 \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Деньги под залог</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">кол-во предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n       \n            <ion-item *ngFor="let item of (data3$ | async)">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button>\n                        <ion-icon name="information-circle"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="card-zag">{{item.company_z}}</div>\n                    <div class="card-all">{{item.int_info_z}}</div>\n                    <div class="card-all">{{item.filter_category_z}}</div>\n                  <ion-buttons end>\n                    <button ion-button>\n                      <ion-icon ios="ios-call" md="md-call"></ion-icon>\n                    </button>\n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n      <br>\n          </ion-slide>\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #5b002d;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/p.png">\n                  </div></div>\n                \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Помощь</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">28 предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n            <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n          </button>\n            <br>\n            <ion-item *ngFor="let item of (data5$ | async)">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button>\n                        <ion-icon name="information-circle"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="card-zag">{{item.company_h}}</div>\n                    <div class="card-all">{{item.int_info_h}}</div>\n                    <div class="card-all">{{item.filter_category_h}}</div>\n                  <ion-buttons end>\n                    <button ion-button>\n                      <ion-icon ios="ios-call" md="md-call"></ion-icon>\n                    </button>\n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n      <br>\n            </ion-slide>\n\n            <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #420042;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/kp.png">\n                  </div></div>\n                  \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Долги списание</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">28 предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button >\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n            <ion-item *ngFor="let item of (data4$ | async)">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button>\n                        <ion-icon name="information-circle"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="card-zag">{{item.company_s}}</div>\n                    <div class="card-all">{{item.int_info_s}}</div>\n                    <div class="card-all">{{item.filter_category_s}}</div>\n                  <ion-buttons end>\n                    <button ion-button>\n                      <ion-icon ios="ios-call" md="md-call"></ion-icon>\n                    </button>\n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n      <br>\n            </ion-slide>\n        </ion-slides>\n\n      \n        \n      </ion-content>   \n\n\n\n\n\n\n\n                             \n\n\n                           \n\n\n\n\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/suitable/suitable.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SuitablePage);
    return SuitablePage;
}());

//# sourceMappingURL=suitable.js.map

/***/ })

},[360]);
//# sourceMappingURL=main.js.map