webpackJsonp([1],{

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPageModule", function() { return ModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal__ = __webpack_require__(720);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModalPageModule = /** @class */ (function () {
    function ModalPageModule() {
    }
    ModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__modal__["a" /* ModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modal__["a" /* ModalPage */]),
            ],
        })
    ], ModalPageModule);
    return ModalPageModule;
}());

//# sourceMappingURL=modal.module.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__help_help__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModalPage = /** @class */ (function () {
    function ModalPage(callNumber, navCtrl, navParams, alertCtrl) {
        this.callNumber = callNumber;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.item = { test: false };
        this.item = navParams.get('item') || { test: true };
    }
    ModalPage.prototype.openLink = function () {
        if (this.item.tel_numb_k == false) {
            window.open(this.item.link_company_k, '_system', 'location=yes');
        }
        else {
            window.open("tel:" + this.item.tel_numb_k, '_system', 'location=yes');
        }
    };
    ModalPage.prototype.closeModal = function () {
        this.navCtrl.pop();
    };
    ModalPage.prototype.goAnOtherPage1 = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__help_help__["a" /* HelpPage */]);
    };
    ModalPage.prototype.dismiss = function (openHintModal) {
        this.viewCtrl.dismiss(openHintModal);
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modal',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/modal/modal.html"*/'<ion-header>\n\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list no-lines="">\n    <div class="login-top">\n\n\n   \n</div> \n<ion-item>\n  <ion-buttons end>\n    <button ion-button icon-only  (click)="closeModal()">\n        <ion-icon item-right name="ios-close-outline"></ion-icon>\n    </button>\n</ion-buttons>\n<br>\n<ion-title style="text-align: left; color: black; font-size: 36px">{{item.company}}</ion-title>\n<div class="card-all"  style="text-align:left; font-size: 16px">{{item?.type_program}}</div>\n<div class="icon_star">\n<ion-icon name="star" style="color:#2e3130;text-align:left "></ion-icon>\n<ion-icon name="star" style="color:#2e3130;text-align:left  "></ion-icon>\n<ion-icon name="star" style="color:#2e3130;text-align:left "></ion-icon>\n<ion-icon name="star" style="color:#2e3130;text-align:left  "></ion-icon>\n<ion-icon name="star-outline" style="color:#2e3130;text-align:left "></ion-icon></div>\n \n<div class="li_spisok">\n \n    <ion-icon ios="ios-disc" md="md-disc"></ion-icon>{{item.int_info}}<br>\n    <ion-icon ios="ios-disc" md="md-disc"></ion-icon>{{item.percent}}<br>\n    <ion-icon ios="ios-disc" md="md-disc"></ion-icon>{{item.theway}}<br>\n \n</div><br>\n<ion-col col-md-3>\n    <button ion-button (click)="openLink()">{{item.text_button}}</button> \n  </ion-col>\n  <div class="card-all"  style="text-align:center;margin-top:15px; font-size: 12px;word-break: break-all">{{item?.age}}</div>\n<ion-grid style="color: black; text-align: left;">\n  <ion-row>\n    \n\n\n\n    <ion-col col-md-3>\n      <ion-icon (click)="goAnOtherPage1()" style="font-size:36px;color:#424141;" ios="ios-medkit-outline" md="md-medkit"></ion-icon>\n    </ion-col>\n    \n    <ion-col col-md-3 offset-md-3>\n      <ion-fab style="font-size:18px">\n        <button  ion-fab color="black"><ion-icon name="md-share"></ion-icon></button>\n        <ion-fab-list side="top">\n          <button ion-fab color="primary"><ion-icon name="logo-vimeo"></ion-icon></button>\n        </ion-fab-list>\n        <ion-fab-list side="bottom">\n          <button ion-fab color="secondary"><ion-icon name="logo-facebook"></ion-icon></button>\n        </ion-fab-list>\n        <ion-fab-list side="left">\n          <button ion-fab color="light"><ion-icon name="logo-googleplus"></ion-icon></button>\n        </ion-fab-list>\n        <ion-fab-list side="right">\n          <button ion-fab color="dark"><ion-icon name="logo-twitter"></ion-icon></button>\n        </ion-fab-list>\n      </ion-fab>\n    </ion-col>\n  \n  </ion-row>\n</ion-grid>\n\n\n\n<br>\n<br>\n\n    \n   </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/modal/modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ })

});
//# sourceMappingURL=1.js.map