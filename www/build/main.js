webpackJsonp([14],{

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DataService = /** @class */ (function () {
    function DataService() {
    }
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DataService);
    return DataService;
}());

//# sourceMappingURL=data.service.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpPage');
    };
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/help/help.html"*/'<!--\n  Generated template for the HelpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>help</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/help/help.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var RegisterPage = /** @class */ (function () {
    function RegisterPage(ofAuth, navCtrl, navParams) {
        this.ofAuth = ofAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
    }
    RegisterPage.prototype.register = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.ofAuth.auth.createUserWithEmailAndPassword(user.email, user.password)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/register/register.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Register</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating>Email</ion-label>\n    <ion-input type="text" [(ngModel)]="user.email"></ion-input>\n    </ion-item>\n    \n    <ion-item>\n        <ion-label floating>Пароль</ion-label>\n        <ion-input type="password" [(ngModel)]="user.password"></ion-input>\n        </ion-item>\n        <button ion-button (click)="register(user)">Регистрация</button>\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_badge__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MessagesPage = /** @class */ (function () {
    function MessagesPage(navCtrl, badge, http, modalCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.badge = badge;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.data102$ = this.http.get('https://fir-auth-c234c.firebaseio.com/notification.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
    }
    MessagesPage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/notification.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    MessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-messages',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/messages/messages.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>\n        Сообщения\n      </ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n  \n      \n\n    \n  \n    <ion-card *ngFor="let item of (data102$ | async);" style=" width: 650px !important;\n    max-width: 98%;\n    display: block;\n    margin: 8px auto 25px;\n    text-align: left;">\n      \n      <ion-card-content> \n          <ion-icon name="redo" item-end style="    display: inline-block;\n          font-size: 1.6em;\n          float: right;\n          color: #232323;"></ion-icon>\n        <img src="/assets/imgs/logo_slide2.png" style=" width: 8%;display: block;"> \n        \n        \n        <div class="title_mess"> {{item.notification_tittle}}</div> \n        \n          <div class="container_showtext" ng-class="{show: show}">{{item.notification_mess}} </div>\n          <div class="text_null"></div>\n      </ion-card-content>\n\n   \n    </ion-card>\n\n<br>\n\n\n<!--\n  <button ion-button (click)="badge.set(10)">Set 10</button>\n  <button ion-button (click)="badge.clear()">Удалить</button>-->\n\n\n\n\n\n\n\n\n    \n\n  \n\n\n\n  \n    </ion-content>\n  \n  \n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/messages/messages.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], MessagesPage);
    return MessagesPage;
}());

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>search</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlidePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_data_service__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SlidePage = /** @class */ (function () {
    function SlidePage(http, navCtrl, navParams, alerCtrl, dataService) {
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alerCtrl = alerCtrl;
        this.dataService = dataService;
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/country/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.appName = 'Ionic App';
        console.log(1);
    }
    SlidePage.prototype.onChangePurpose = function (purpose) {
        console.log(purpose);
    };
    SlidePage.prototype.onChangeCountry = function (item) {
        switch (item.id) {
            case 1001:
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_rus/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
                break;
            case 1002:
                console.log('kaz');
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_kaz/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
                break;
            case 1003:
                console.log('ukr');
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_ukr/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
                break;
        }
    };
    SlidePage.prototype.goAnOtherPage = function () {
        this.dataService.storedData = { purpose: this.purpose, city: this.region.id_citys };
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    SlidePage.prototype.onChangeRegion = function (region) {
        this.region = region;
    };
    SlidePage.prototype.next = function () {
        this.slides.slideTo(2, 500);
    };
    SlidePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/country.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/city_rus/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */]) === "function" && _a || Object)
    ], SlidePage.prototype, "slides", void 0);
    SlidePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-slide',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/slide/slide.html"*/'<ion-content>\n    <ion-slides pager>\n\n        <ion-slide style="background-color: rgb(25, 25, 25); ">\n            <div class="text_slide">\n                <div class="hello"> Привет!\n\n                </div>\n\n                <h5 style="color:white;">Мы поможем тебе <br>\n                    легко найти деньги</h5>\n\n                <br>\n                <div class="logo_slide">\n                    <img src="./assets/imgs/logo_slide2.png">\n                </div>\n                <br>\n\n                <h5 style="color:white;">Для поиска денег<br>нажмите далее</h5>\n                <br>\n                <button ion-button color="light" (click)="next()">Далее</button>\n            </div>\n        </ion-slide>\n\n\n        <ion-slide #slide2 style="background-color: rgb(25, 25, 25);">\n            <div class="hello_text">\n                <h5 style="color:white;">СПРАВОЧНИК КРЕДИТОРОВ<br>\n                    поможет легко найти деньги</h5>\n            </div>\n\n            <br>\n            <ion-label style="color:#ffffff;  font-size: 1.2em; font-weight: 600;"></ion-label>\n            <ion-item style="background-color: rgb(25, 25, 25);">\n                <ion-select class="region" placeholder="Я ищу" [(ngModel)]="purpose" (ngModelChange)="onChangePurpose($event)">\n                    <ion-option value="mond">Деньги без залога</ion-option>\n                    <ion-option value="monp">Деньги под залог</ion-option>\n                    <ion-option value="cred">Помощь в кредите</ion-option>\n                    <ion-option value="nom">Избавление от кредитов</ion-option>\n           \n                </ion-select>\n            </ion-item>\n\n            <br>\n            <ion-label style="color:#ffffff;  font-size: 1.2em; font-weight: 600;"></ion-label>\n            <ion-item style="background-color: rgb(25, 25, 25);">\n                <br>\n                <ion-select class="region" placeholder="Страна" [(ngModel)]="country" (ngModelChange)="onChangeCountry($event)">\n                    <ion-option [value]="item" *ngFor="let item of (data1$ | async)">{{item.country}}  </ion-option>\n                </ion-select>\n\n            </ion-item>\n            <br>\n            <br>\n\n\n            <ion-item style="background-color: rgb(25, 25, 25);">\n\n\n                <ion-select class="region" placeholder="Регион" [(ngModel)]="country" (ngModelChange)="onChangeRegion($event)">\n                    <ion-option [value]="item" *ngFor="let item of (data2$ | async)">{{item.all_city}}  </ion-option>\n                </ion-select>\n\n            </ion-item>\n            <br>\n            <div class="hello_text">\n                <h5 style="color:white;">Выберите страну/регион <br>\n                    и нажмите "Далее"</h5>\n            </div>\n            <br>\n            <button ion-button color="light" (click)="goAnOtherPage()">ИСКАТЬ</button>\n\n\n        </ion-slide>\n\n\n    </ion-slides>\n\n</ion-content>\n  '/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/slide/slide.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* ChangeDetectionStrategy */].OnPush,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_data_service__["a" /* DataService */]) === "function" && _f || Object])
    ], SlidePage);
    return SlidePage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=slide.js.map

/***/ }),

/***/ 172:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 172;

/***/ }),

/***/ 216:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/account/account.module": [
		706,
		13
	],
	"../pages/contact/contact.module": [
		707,
		12
	],
	"../pages/favorites/favorites.module": [
		708,
		11
	],
	"../pages/help/help.module": [
		710,
		10
	],
	"../pages/home/home.module": [
		709,
		9
	],
	"../pages/login/login.module": [
		711,
		8
	],
	"../pages/messages/messages.module": [
		712,
		7
	],
	"../pages/modal/modal.module": [
		713,
		1
	],
	"../pages/personal/personal.module": [
		714,
		0
	],
	"../pages/podhzayvka/podhzayvka.module": [
		715,
		6
	],
	"../pages/register/register.module": [
		717,
		5
	],
	"../pages/search/search.module": [
		716,
		4
	],
	"../pages/slide/slide.module": [
		718,
		3
	],
	"../pages/suitable/suitable.module": [
		719,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 216;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__suitable_suitable__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__help_help__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__messages_messages__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__suitable_suitable__["a" /* SuitablePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_5__messages_messages__["a" /* MessagesPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_4__help_help__["a" /* HelpPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/tabs/tabs.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Деньги в долг срочно</ion-title>\n    <ion-buttons end>  \n        <button class="contact_avatar" name="contact"(click)="openPopupPage()">\n          <ion-icon name="contact"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-tabs>\n\n  <ion-tab [root]="tab2Root" tabTitle="Кредиторы" tabIcon="keypad"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Поиск" tabIcon="search"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Сообщения" tabIcon="text"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="Помощь" tabIcon="medkit"></ion-tab>\n  \n</ion-tabs>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AccountPage = /** @class */ (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-account',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/account/account.html"*/'<!--\n  Generated template for the AccountPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>account</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/account/account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/contact/contact.html"*/'<!--\n  Generated template for the ContactPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>contact</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/contact/contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FavoritesPage = /** @class */ (function () {
    function FavoritesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FavoritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritesPage');
    };
    FavoritesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-favorites',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/favorites/favorites.html"*/'<!--\n  Generated template for the FavoritesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>favorites</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/favorites/favorites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FavoritesPage);
    return FavoritesPage;
}());

//# sourceMappingURL=favorites.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__suitable_suitable__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.pushPage = __WEBPACK_IMPORTED_MODULE_5__suitable_suitable__["a" /* SuitablePage */];
        this.SwipedTabsIndicator = null;
        this.tabs = [];
        this.data7$ = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/creditallceli/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dengi_podzalog/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgispisanie/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim_card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["shareReplay"])(1));
        this.loadData();
    }
    /*
      Открытие страницы оформления займа ТЕСТ
       */
    HomePage.prototype.openPodhzayvkaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]);
    };
    HomePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json');
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>Home</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  \n\n\n  <ion-content padding >\n     \n    \n    <br>\n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Подходящее для вас</h4>\n  \n  \n\n\n\n\n    \n  \n    <ion-scroll scrollX="true" style="white-space:nowrap;height: 150px;background-color: #ffffff;">\n     \n   \n    \n      <ion-card  style="background-color:#ffffff">\n        <img  src="./assets/slide/all/zaem.png"  tapped [navPush]="pushPage" [navParams]="{initialSlide: 0}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/credit-cart.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 1}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/credit-all.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 2}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff"> \n        <img src="./assets/slide/all/credit-zalog.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 3}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n    \n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/help-credit.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 4}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n      <ion-card style="background-color:#ffffff">\n        <img src="./assets/slide/all/dolg.png"   tapped [navPush]="pushPage" [navParams]="{initialSlide: 5}">\n        <div class="card-title">12 предложений</div>\n        <div class="card-subtitle">доступно сейчас</div>\n      </ion-card>\n    </ion-scroll>\n  \n  \n  \n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Интересное</h4>\n  \n    <ion-scroll scrollX="true" style="white-space:nowrap;width:100%;height: 185px;background-color: #ffffff;">\n      <ion-card class="card_int" nowrap *ngFor="let item of items" style="width:100% ;padding:10px;height:100%"><h4 text-center style="font-size: 1.2em; font-weight: 600;">{{item.company_h}}</h4>\n        <ion-grid>\n          <ion-row text-center>\n            <ion-col col-md-3>\n           18+\n            </ion-col>\n            <ion-col >\n                <ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon>\n                <ion-icon name="star-outline"></ion-icon>\n            </ion-col>\n            <ion-col col-md-3 offset-md-3>\n             <ion-icon name="heart"></ion-icon>\n            </ion-col>\n          </ion-row>\n          <button  style ="text-align: center;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button color="secondary">Оформить</button>\n          <h3 text-center>{{item.link_company_h}}</h3>\n          <h6 text-center>{{item.int_info_h}}</h6>\n    \n    \n        </ion-grid>\n      </ion-card>\n        </ion-scroll>\n  \n  \n    <h4 text-center style="font-size: 1.3em; font-weight: 600;">Все категории</h4>\n  \n    <ion-grid>\n      <ion-row justify-content-start>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 0}">\n          <img src="./assets/slide/all/card.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 1}">\n          <img src="./assets/slide/all/card2.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 2}">\n          <img src="./assets/slide/all/card3.png">\n        </ion-col>\n        <ion-col col-6 col-sm tapped [navPush]="pushPage" [navParams]="{initialSlide: 3}">\n          <img src="./assets/slide/all/card4.png">\n        </ion-col>\n        <ion-col col-6 col-sm  tapped [navPush]="pushPage" [navParams]="{initialSlide: 4}">\n          <img src="./assets/slide/all/card5.png">\n        </ion-col>\n        <ion-col col-6 col-sm  tapped [navPush]="pushPage" [navParams]="{initialSlide: 5}">\n          <img src="./assets/slide/all/card6.png">\n        </ion-col>\n  \n  \n  \n      </ion-row>\n    </ion-grid>\n    \n\n  \n  \n  </ion-content>'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__register_register__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var LoginPage = /** @class */ (function () {
    function LoginPage(ofAuth, navCtrl, navParams) {
        this.ofAuth = ofAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
    }
    LoginPage.prototype.login = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    result = this.ofAuth.auth.signInWithEmailAndPassword(user.email, user.password);
                    console.log(result);
                    if (result) {
                        this.navCtrl.setRoot("PersonalPage");
                    }
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/login/login.html"*/'\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-item>\n<ion-label floating>Email</ion-label>\n<ion-input type="text" [(ngModel)]="user.email"></ion-input>\n</ion-item>\n\n<ion-item>\n    <ion-label floating>Пароль</ion-label>\n    <ion-input type="password" [(ngModel)]="user.password"></ion-input>\n    </ion-item>\n\n<button ion-button (click)="login(user)">Логин</button>\n<button ion-button  color="light" (click)="register()">Регистрация</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(369);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_firebase_config__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(704);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(705);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_help_help__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_account_account__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_login_login__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_register_register__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_slide_slide__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_favorites_favorites__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_search_search__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_suitable_suitable__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_messages_messages__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_podhzayvka_podhzayvka__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_data_service__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_19__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_suitable_suitable__["a" /* SuitablePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_slide_slide__["a" /* SlidePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_favorites_favorites__["a" /* FavoritesPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favorites/favorites.module#FavoritesPageModule', name: 'FavoritesPage', segment: 'favorites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help/help.module#HelpPageModule', name: 'HelpPage', segment: 'help', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/messages/messages.module#MessagesPageModule', name: 'MessagesPage', segment: 'messages', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal/modal.module#ModalPageModule', name: 'ModalPage', segment: 'modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/personal/personal.module#PersonalPageModule', name: 'PersonalPage', segment: 'personal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/podhzayvka/podhzayvka.module#PodhzayvkaPageModule', name: 'PodhzayvkaPage', segment: 'podhzayvka', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slide/slide.module#SlidePageModule', name: 'SlidePage', segment: 'slide', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/suitable/suitable.module#SuitablePageModule', name: 'SuitablePage', segment: 'suitable', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_0__app_firebase_config__["a" /* FIREBASE_CONFIG */]),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_23__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__pages_suitable_suitable__["a" /* SuitablePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_slide_slide__["a" /* SlidePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_favorites_favorites__["a" /* FavoritesPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */],
                __WEBPACK_IMPORTED_MODULE_25__services_data_service__["a" /* DataService */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyB2M6KvNtii5HuCiwVdjOjCAxadVv87S8U",
    authDomain: "fir-auth-c234c.firebaseapp.com",
    databaseURL: "https://fir-auth-c234c.firebaseio.com",
    projectId: "fir-auth-c234c",
    storageBucket: "fir-auth-c234c.appspot.com",
    messagingSenderId: "697092668767"
};
//# sourceMappingURL=app.firebase.config.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = isNullOrUndefined;
function isNullOrUndefined(value) {
    return value === undefined || value === null;
}
//# sourceMappingURL=is-null-or-undefined.js.map

/***/ }),

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_slide_slide__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_slide_slide__["a" /* SlidePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.openPage = function (page) {
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PodhzayvkaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PodhzayvkaPage = /** @class */ (function () {
    function PodhzayvkaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PodhzayvkaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-podhzayvka',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/podhzayvka/podhzayvka.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>posdfsdfdhzayvka</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\nsadfsdfadsf\n</ion-content>\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/podhzayvka/podhzayvka.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], PodhzayvkaPage);
    return PodhzayvkaPage;
}());

//# sourceMappingURL=podhzayvka.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuitablePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_service__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SuitablePage = /** @class */ (function () {
    function SuitablePage(alertController, modalCtrl, http, navCtrl, navParams, dataService) {
        this.alertController = alertController;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.SwipedTabsIndicator = null;
        this.tabs = [];
        this.initialSlide = 0;
        this.cityId = Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(this.dataService.storedData) ? 0 : this.dataService.storedData.city;
        this._initData();
        this.tabs = ['Займы', 'Карты', 'Кредиты', 'Деньги', 'Помощь', 'Долги'];
        switch (this.dataService.storedData.purpose) {
            case 'mond':
                this.initialSlide = 3;
                break;
            case 'monp':
                this.initialSlide = 1;
                break;
            case 'cred':
                this.initialSlide = 4;
                break;
            case 'nom':
                this.initialSlide = 5;
                break;
            case 'allvs':
                this.initialSlide = 0;
                break;
            default:
                this.initialSlide = 0;
                break;
        }
    }
    SuitablePage.prototype.openModal = function () {
        var myModalOptions = {
            enableBackdropDismiss: false,
        };
        var myModal = this.modal.create('ModalPage', { data: myModalData }, myModalOptions);
        myModal.present();
    };
    SuitablePage.prototype.ionViewDidEnter = function () {
        this.SwipedTabsIndicator = document.getElementById('indicator');
    };
    SuitablePage.prototype.selectTab = function (index) {
        this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (100 * index) + '%,0,0)';
        this.SwipedTabsSlider.slideTo(index, 500);
    };
    SuitablePage.prototype.updateIndicatorPosition = function () {
        // this condition is to avoid passing to incorrect index
        if (this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
            //	this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
        }
    };
    SuitablePage.prototype.animateIndicator = function ($event) {
        if (this.SwipedTabsIndicator) {
            this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress * (this.SwipedTabsSlider.length() - 1)) * 100) + '%,0,0)';
        }
    };
    /*
      Открытие страницы оформления займа ТЕСТ
       */
    SuitablePage.prototype.openPodhzayvkaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__podhzayvka_podhzayvka__["a" /* PodhzayvkaPage */]);
    };
    SuitablePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/credit/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/podzalog/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgi.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    SuitablePage.prototype.openHintModal = function (item) {
        console.log('openHintModal');
        console.log(item);
        var modalOptions = {
            cssClass: 'inset-modal',
        };
        var modal = this.modalCtrl.create('ModalPage', { item: item }, modalOptions);
        modal.present();
        console.log('Cancel clicked');
    };
    SuitablePage.prototype._initData = function () {
        var _this = this;
        this.data7$ = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/card/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/podzalog/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgi.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
        this.data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim/.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (item) {
            if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_is_null_or_undefined__["a" /* isNullOrUndefined */])(item)) {
                return item;
            }
            return item.filter(function (city) { return city.city === _this.cityId; });
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["shareReplay"])(1));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('SwipedTabsSlider'),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */]) === "function" && _a || Object)
    ], SuitablePage.prototype, "SwipedTabsSlider", void 0);
    SuitablePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-suitable',template:/*ion-inline-start:"/Users/webpony/ionic/money2/src/pages/suitable/suitable.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding >\n\n\n        <ion-slides #SwipedTabsSlider  (ionSlideDrag)="animateIndicator($event)"\n                    (ionSlideWillChange)="updateIndicatorPosition()"\n                    (ionSlideDidChange)="updateIndicatorPosition()"\n                    (pan)="updateIndicatorPosition()"\n                    [initialSlide]="initialSlide"\n                    [pager]="false"\n        >\n       <!-- here is our dynamic line  "indicator"-->\n       <div id=\'indicator\'  class="SwipedTabs-indicatorSegment" [ngStyle]="{\'width.%\': (100/this.tabs.length)}"></div>\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs">\n                  <ion-segment-button  *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n      \n\n              <div class="letter_head" style="background: #006699;height: 100px;max-height: 96%;">\n                  <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                        <img src="./assets/imgs/z.png">\n                      </div></div>\n                      \n              <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Займы</h4>\n              <p text-center style="font-size: 1.0em; line-height: 1.5;">все</p>\n\n              <br> \n              <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n                <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n              </button>\n                   \n            <ion-item *ngFor="let item of (data6$ | async); let i = index">\n              <ion-toolbar>\n                  <ion-buttons end>\n                      <button ion-button ion-button  (click)="openHintModal(item)" >\n                        <ion-icon name="share-alt"></ion-icon>\n                      </button>\n                    </ion-buttons>\n                    <div class="cardcom_id">{{i + 1}}</div>\n                    <div class="card-zag">{{item.type}}</div>\n                    <div class="card-alls">{{item.int_info}}</div>\n                    <div class="card-all">{{item.filter_category}}</div>\n                \n                  <ion-buttons end>\n                  \n                  </ion-buttons>\n                </ion-toolbar>\n      \n       </ion-item>\n             \n             \n          <br>\n          </ion-slide>\n\n          <ion-slide >\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n    \n              <div class="letter_head" style="background: #006666;height: 100px;max-height: 96%;">\n                  <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                        <img src="./assets/imgs/kg.png">\n                      </div></div>\n                      \n              <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Карты</h4>\n              <p text-center style="font-size: 1.0em; line-height: 1.5;">все</p>\n              <br>\n              <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n                                    <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n                </button>\n                \n                <ion-item *ngFor="let item of (data1$ | async); let i = index">\n                    <ion-toolbar>\n                        <ion-buttons end>\n                            <button ion-button ion-button  (click)="openHintModal(item)" >\n                              <ion-icon name="share-alt"></ion-icon>\n                            </button>\n                          </ion-buttons>\n                          <div class="cardcom_id">{{i + 1}}</div>\n                          <div class="card-zag">{{item.company}}</div>\n                          <div class="card-alls">{{item.int_info}}</div>\n                          <div class="card-all">{{item.filter_category}}</div>\n                      \n                        <ion-buttons end>\n                        \n                        </ion-buttons>\n                      </ion-toolbar>\n          \n           </ion-item>\n          <br>\n          </ion-slide>\n\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #006666;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/k.png">\n                  </div></div>\n                  \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Кредиты</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">кол-во предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n            <ion-item *ngFor="let item of (data2$ | async); let i = index">\n                <ion-toolbar>\n                    <ion-buttons end>\n                        <button ion-button ion-button  (click)="openHintModal(item)" >\n                          <ion-icon name="share-alt"></ion-icon>\n                        </button>\n                      </ion-buttons>\n                      <div class="cardcom_id">{{i + 1}}</div>\n                      <div class="card-zag">{{item.company}}</div>\n                      <div class="card-alls">{{item.int_info}}</div>\n                      <div class="card-all">{{item.filter_category}}</div>\n                  \n                    <ion-buttons end>\n                    \n                    </ion-buttons>\n                  </ion-toolbar>\n      \n      \n       </ion-item>\n      <br>\n          </ion-slide>\n\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #65361b;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/d.png">\n                  </div></div>\n                 \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Деньги под залог</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">кол-во предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n       \n            <ion-item *ngFor="let item of (data3$ | async); let i = index">\n                <ion-toolbar>\n                    <ion-buttons end>\n                        <button ion-button ion-button  (click)="openHintModal(item)" >\n                          <ion-icon name="share-alt"></ion-icon>\n                        </button>\n                      </ion-buttons>\n                      <div class="cardcom_id">{{i + 1}}</div>\n                      <div class="card-zag">{{item.company}}</div>\n                      <div class="card-alls">{{item.int_info}}</div>\n                      <div class="card-all">{{item.filter_category}}</div>\n                  \n                    <ion-buttons end>\n                    \n                    </ion-buttons>\n                  </ion-toolbar>\n      \n      \n       </ion-item>\n      <br>\n          </ion-slide>\n\n          <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #5b002d;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/p.png">\n                  </div></div>\n                \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Помощь</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">28 предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button>\n            <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n          </button>\n            <br>\n            <ion-item *ngFor="let item of (data5$ | async); let i = index">\n                <ion-toolbar>\n                    <ion-buttons end>\n                        <button ion-button ion-button  (click)="openHintModal(item)" >\n                          <ion-icon name="share-alt"></ion-icon>\n                        </button>\n                      </ion-buttons>\n                      <div class="cardcom_id">{{i + 1}}</div>\n                      <div class="card-zag">{{item.company}}</div>\n                      <div class="card-alls">{{item.int_info}}</div>\n                      <div class="card-all">{{item.filter_category}}</div>\n                  \n                    <ion-buttons end>\n                    \n                    </ion-buttons>\n                  </ion-toolbar>\n      \n       </ion-item>\n      <br>\n            </ion-slide>\n\n            <ion-slide>\n              <ion-segment  class="SwipedTabs-tabs"  >\n                  <ion-segment-button *ngFor=\'let tab of tabs ; let i = index \' value="IngoreMe" (click)="selectTab(i)"\n                  [ngClass]=\'{ "SwipedTabs-activeTab" : ( this.SwipedTabsSlider  && ( this.SwipedTabsSlider.getActiveIndex() === i || (  tabs.length -1 === i&& this.SwipedTabsSlider.isEnd()))) }\' >\n                    {{tab}}\n                  </ion-segment-button>\n                </ion-segment>\n            <div class="letter_head" style="background: #420042;height: 100px;max-height: 96%;">\n              <div class="letter" style="width: 100px; margin: auto;max-height: 90%;display: block;">\n                    <img src="./assets/imgs/kp.png">\n                  </div></div>\n                  \n          <h4 text-center style="font-size: 1.5em; font-weight: 600; line-height: 0.5;">Долги списание</h4>\n          <p text-center style="font-size: 1.0em; line-height: 0.5;">все</p>\n          <p text-center style="font-size: 1.0em; line-height: 1.5;">28 предложений</p>\n          <br>\n          <button style ="text-align: center;    font-size: 10px;margin-left: auto; margin-right: auto; display: block; background:#006699" ion-button >\n              <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n            </button>\n            \n            <ion-item *ngFor="let item of (data4$ | async); let i = index">\n                <ion-toolbar>\n                    <ion-buttons end>\n                        <button ion-button ion-button  (click)="openHintModal(item)" >\n                          <ion-icon name="share-alt"></ion-icon>\n                        </button>\n                      </ion-buttons>\n                      <div class="cardcom_id">{{i + 1}}</div>\n                      <div class="card-zag">{{item.company}}</div>\n                      <div class="card-alls">{{item.int_info}}</div>\n                      <div class="card-all">{{item.filter_category}}</div>\n                  \n                    <ion-buttons end>\n                    \n                    </ion-buttons>\n                  </ion-toolbar>\n      \n      \n       </ion-item>\n      <br>\n            </ion-slide>\n        </ion-slides>\n\n      \n        \n      </ion-content>   \n\n\n\n\n\n\n\n                             \n\n\n                           \n\n\n\n\n'/*ion-inline-end:"/Users/webpony/ionic/money2/src/pages/suitable/suitable.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_data_service__["a" /* DataService */]) === "function" && _g || Object])
    ], SuitablePage);
    return SuitablePage;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=suitable.js.map

/***/ })

},[364]);
//# sourceMappingURL=main.js.map