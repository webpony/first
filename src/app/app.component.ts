import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SlidePage } from '../pages/slide/slide';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AccountPage } from '../pages/account/account';
import { Nav } from 'ionic-angular';
import { FavoritesPage } from '../pages/favorites/favorites';
import { SearchPage } from '../pages/search/search';
import { SuitablePage } from '../pages/suitable/suitable';
import { MessagesPage } from '../pages/messages/messages';
import { HomePage } from '../pages/home/home';
import { ContactPage } from '../pages/contact/contact';

import { PodhzayvkaPage } from '../pages/podhzayvka/podhzayvka';
import { PersonalPage } from '../pages/personal/personal';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SlidePage;
  openPage(page) {
 
  }
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
