
import { FIREBASE_CONFIG } from './app.firebase.config';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { ContactPage } from '../pages/contact/contact';
import { HelpPage} from '../pages/help/help';
import { HomePage } from '../pages/home/home';
import { Badge } from '@ionic-native/badge';
import { TabsPage } from '../pages/tabs/tabs';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SlidePage } from '../pages/slide/slide';
import { FavoritesPage } from '../pages/favorites/favorites';
import { SearchPage } from '../pages/search/search';
import { SuitablePage} from '../pages/suitable/suitable';
import { MessagesPage} from '../pages/messages/messages';
import { PodhzayvkaPage} from '../pages/podhzayvka/podhzayvka';
import {HttpClientModule} from '@angular/common/http';
import { CallNumber } from '@ionic-native/call-number';
import { DataService } from '../services/data.service';
 
@NgModule({
  declarations: [
    MyApp,
    SearchPage,
    SuitablePage,
    HomePage,
    HelpPage,
    AccountPage,
    LoginPage,
    RegisterPage,
    SlidePage,
    ContactPage,
    FavoritesPage,
    MessagesPage,
    TabsPage,
    PodhzayvkaPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SuitablePage,
    MessagesPage,
    HomePage,
    HelpPage,
    SearchPage,
    SlidePage,
    ContactPage,
     LoginPage,
     RegisterPage,
     AccountPage,
     FavoritesPage,
    TabsPage,
    PodhzayvkaPage
  ],
  providers: [
    StatusBar,
    Badge,
    DataService,
    CallNumber,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
