import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PodhzayvkaPage } from './podhzayvka';

@NgModule({
  declarations: [
    PodhzayvkaPage,
  ],
  imports: [
    IonicPageModule.forChild(PodhzayvkaPage),
  ],
})
export class PodhzayvkaPageModule {}
