import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { shareReplay } from 'rxjs/operators';
import { Country, Region } from '../../models/';
import { DataService } from '../../services/data.service';

@IonicPage()
@Component({
    selector: 'page-slide',
    templateUrl: 'slide.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SlidePage {
    @ViewChild(Slides) slides: Slides;
    items: any;
    purpose: string;


    data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/country/.json').pipe(shareReplay(1));
    data2$;


    appName = 'Ionic App';
    anOtherPage: TabsPage;
    navctrl: any;
    country: Country;
    region: Region;

    constructor(public http: HttpClient,
                public navCtrl: NavController,
                public navParams: NavParams,
                public alerCtrl: AlertController,
                private dataService: DataService) {console.log(1); }

    onChangePurpose(purpose: string): void {
        console.log(purpose);
    }

    onChangeCountry(item: Country): void {
        switch (item.id) {
            case 1001:
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_rus/.json').pipe(shareReplay(1));
                break;
            case 1002:
                console.log('kaz');
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_kaz/.json').pipe(shareReplay(1));
                break;
            case 1003:
                console.log('ukr');
                this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/city_ukr/.json').pipe(shareReplay(1));
                break;
        }
    }

    goAnOtherPage() {
        this.dataService.storedData = {purpose: this.purpose, city: this.region.id_citys};
        this.navCtrl.setRoot(TabsPage);
    }

    onChangeRegion(region: Region) {
        this.region = region;
    }

    next() {
        this.slides.slideTo(2, 500);
    }


    loadData() {
        let data: Observable<any>;

        data = this.http.get('https://fir-auth-c234c.firebaseio.com/country.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/city_rus/.json').pipe(shareReplay(1));

        data.subscribe(result => {
            this.items = result;

        });
    }


}