import { RegisterPage } from './../register/register';
import { User } from './../../models/user';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{ AngularFireAuth} from'angularfire2/auth';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

user = {} as User;

  constructor( public ofAuth : AngularFireAuth,
    public navCtrl: NavController, public navParams: NavParams) {
  }

async login(user : User) {
    try {
    const result = this.ofAuth.auth.signInWithEmailAndPassword(user.email, user.password)
  console.log (result);
  if (result) {
  this.navCtrl.setRoot("PersonalPage");
  }
}
  catch (e){

    console.log (e)
  }
}

register() {
this.navCtrl.push(RegisterPage);
  
}
}
