import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';




@IonicPage()
@Component({
  selector: 'page-personal',
  templateUrl: 'personal.html',
})
export class PersonalPage {
  ToastController: any;

  constructor( public ofAuth : AngularFireAuth,public toast :ToastController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillload(){
  this.ofAuth.authState.subscribe(data => {

  if (data && data.email && data.uid) {

  this.toast.create({
  message :'Вы зарегистрированы to APP_NAME, $ {data.email}',
  duration :3000
  }).present();

  } 
   else {
    this.toast.create({
      message :'Вы не зарегистрированы',
      duration :3000
 
}).present();
}

});
}
}
