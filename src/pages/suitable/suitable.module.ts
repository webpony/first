import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuitablePage } from './suitable';
import { NavController, NavParams } from 'ionic-angular';

import { PodhzayvkaPage} from '../podhzayvka/podhzayvka';

@NgModule({
  declarations: [
    SuitablePage,
  ],
  imports: [
    IonicPageModule.forChild(SuitablePage),
  ],
})

export class SuitablePageModule {
  anOtherPage: PodhzayvkaPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
}



 

/*
  Открытие страницы оформления займа ТЕСТ
   */
  openPodhzayvkaPage() {
    this.navCtrl.push(PodhzayvkaPage);
  }
}