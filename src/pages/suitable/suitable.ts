import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, AlertController } from 'ionic-angular';
import { PodhzayvkaPage } from '../podhzayvka/podhzayvka';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { shareReplay, switchMap, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { isNullOrUndefined } from '../../utils/is-null-or-undefined';
import { DataService } from '../../services/data.service';

interface Offer {
    /** Айдишник предложения */
    id: number;
    name: string;
    discount: number;
}

@IonicPage()
@Component({
    selector: 'page-suitable',
    templateUrl: 'suitable.html',
})
export class SuitablePage {
    anOtherPage: PodhzayvkaPage;
    @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides;

    public items: any;

    SwipedTabsIndicator: any = null;
    tabs: any = [];
    cityId: number;

    initialSlide = 0;
    data7$;
    data1$;
    data2$;
    data3$;
    data4$;
    data5$;
    data6$;
    modal: any;
    viewCtrl: any;
    alertCtrl: any;

    constructor(
        public alertController: AlertController,
        public modalCtrl: ModalController,
        public http: HttpClient,
        public navCtrl: NavController,
        public navParams: NavParams,
        public dataService: DataService) {

        this.cityId = isNullOrUndefined(this.dataService.storedData) ? 0 : this.dataService.storedData.city;
        this._initData();
        this.tabs = ['Займы', 'Карты', 'Кредиты', 'Деньги', 'Помощь', 'Долги'];
        switch (this.dataService.storedData.purpose) {
            case 'mond':
                this.initialSlide = 3;
                break;
            case 'monp':
                this.initialSlide = 1;
                break;
            case 'cred':
                this.initialSlide = 4;
                break;
            case 'nom':
                this.initialSlide = 5;
                break;
            case 'allvs':
                this.initialSlide = 0;
                break;
            default:
                this.initialSlide = 0;
                break;
        }
    }


    openModal() {

        const myModalOptions: ModalOptions = {
            enableBackdropDismiss: false,
        };

       
        const myModal: Modal = this.modal.create('ModalPage', { data: myModalData }, myModalOptions);

        myModal.present();

     
       

    }

    ionViewDidEnter() {
        this.SwipedTabsIndicator = document.getElementById('indicator');
    }

    selectTab(index) {
        this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (100 * index) + '%,0,0)';
        this.SwipedTabsSlider.slideTo(index, 500);
    }

    updateIndicatorPosition() {
        // this condition is to avoid passing to incorrect index
        if (this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
            //	this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
        }

    }

    animateIndicator($event) {
        if (this.SwipedTabsIndicator) {
            this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress * (this.SwipedTabsSlider.length() - 1)) * 100) + '%,0,0)';
        }
    }


    /*
      Открытие страницы оформления займа ТЕСТ
       */
    openPodhzayvkaPage() {
        this.navCtrl.push(PodhzayvkaPage);
    }

    loadData() {
        let data: Observable<any>;

        data = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/card/.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/credit/.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/podzalog/.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgi.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(shareReplay(1));
        data = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim/.json').pipe(shareReplay(1));

        data.subscribe(result => {
            this.items = result;

        });
    }


    openHintModal(item: any) {
        console.log('openHintModal');

        console.log(item);
        const modalOptions: ModalOptions = {
            cssClass: 'inset-modal',
        };
        const modal = this.modalCtrl.create('ModalPage', { item: item }, modalOptions);
        modal.present();
        console.log('Cancel clicked');
    }

    private _initData() {
        this.data7$ = this.http.get('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/card/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/podzalog/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgi.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
        this.data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim/.json').pipe(
            map(item => {
                if (isNullOrUndefined(item)) {
                    return item;
                }
                return (item as Array<any>).filter(city => city.city === this.cityId);
            }),
            shareReplay(1),
        );
    }

}
