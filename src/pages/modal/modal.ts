import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HelpPage} from '../help/help';
import { CallNumber } from '@ionic-native/call-number';
import { text } from '@angular/core/src/render3/instructions';
import { linkToSegment } from 'ionic-angular/umd/navigation/nav-util';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  item : any = {test: false};
  viewCtrl: any;
 
  constructor(private callNumber: CallNumber,public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController) {
    this.item = navParams.get('item') || {test: true};
  }


openLink(){
  if (this.item.tel_numb_k == false) {
  window.open(this.item.link_company_k,'_system', 'location=yes');
  }
else {
  window.open("tel:" + this.item.tel_numb_k,'_system', 'location=yes');
}
 
}
closeModal() {
  this.navCtrl.pop();

}
goAnOtherPage1() {
  this.navCtrl.setRoot(HelpPage);
}
 
dismiss(openHintModal) {
  
  this.viewCtrl.dismiss(openHintModal);
  }
}




