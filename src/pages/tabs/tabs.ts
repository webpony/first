import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { IonicPage, NavController, NavParams,Slides } from 'ionic-angular';
import { SuitablePage } from '../suitable/suitable';
import { SearchPage } from '../search/search';
import { HelpPage } from '../help/help';
import { MessagesPage } from '../messages/messages';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {


  tab2Root = SuitablePage;
  tab3Root = SearchPage;
  tab4Root = MessagesPage;
  tab5Root = HelpPage;

  constructor(public navCtrl: NavController,) {

  }

 
}
