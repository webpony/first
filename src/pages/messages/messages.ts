import { Badge } from '@ionic-native/badge';
import { Component } from '@angular/core';
import { NavController, ModalController, IonicPage, AlertController } from 'ionic-angular';
import { shareReplay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  
  items: any;
    purpose: string;


    data102$ = this.http.get('https://fir-auth-c234c.firebaseio.com/notification.json').pipe(shareReplay(1));


  constructor( public navCtrl: NavController,private badge: Badge,public http: HttpClient,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,) {
   
  }
 loadData() {
  let data: Observable<any>;

  data = this.http.get('https://fir-auth-c234c.firebaseio.com/notification.json').pipe(shareReplay(1));


  data.subscribe(result => {
      this.items = result;

  });
}


  
}


