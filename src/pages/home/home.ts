
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PodhzayvkaPage} from '../podhzayvka/podhzayvka';
import {HttpClient}from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { shareReplay, switchMap, startWith } from 'rxjs/operators';


import { SuitablePage } from '../suitable/suitable';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  anOtherPage: PodhzayvkaPage;
  pushPage = SuitablePage;

  
  public items:any;
  nav: any;
  SwipedTabsIndicator :any= null;
  tabs:any=[];

  
  data7$ = this.http.get ('https://fir-auth-c234c.firebaseio.com/interesting/.json').pipe(shareReplay(1))
  data1$ = this.http.get('https://fir-auth-c234c.firebaseio.com/credit_card/.json').pipe(shareReplay(1))
  data2$ = this.http.get('https://fir-auth-c234c.firebaseio.com/creditallceli/.json').pipe(shareReplay(1))
  data3$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dengi_podzalog/.json').pipe(shareReplay(1))
  data4$ = this.http.get('https://fir-auth-c234c.firebaseio.com/dolgispisanie/.json').pipe(shareReplay(1)) 
  data5$ = this.http.get('https://fir-auth-c234c.firebaseio.com/help/.json').pipe(shareReplay(1))
  data6$ = this.http.get('https://fir-auth-c234c.firebaseio.com/zaim_card/.json').pipe(shareReplay(1))




  constructor(public navCtrl: NavController, public navParams: NavParams,public http:HttpClient) {

   this.loadData (); 
   }

  
/*
  Открытие страницы оформления займа ТЕСТ
   */
  openPodhzayvkaPage() {
    this.navCtrl.push(PodhzayvkaPage);
  }
  loadData () {
    let data:Observable<any>;


    data = this.http.get ('https://fir-auth-c234c.firebaseio.com/interesting/.json');
    

      data.subscribe(result => {
   this.items = result;
   
       })
      }
    }
  


 



